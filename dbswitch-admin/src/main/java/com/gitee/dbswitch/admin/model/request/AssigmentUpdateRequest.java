// Copyright tang.  All rights reserved.
// https://gitee.com/inrgihc/dbswitch
//
// Use of this source code is governed by a BSD-style license
//
// Author: tang (inrgihc@126.com)
// Date : 2020/1/2
// Location: beijing , china
/////////////////////////////////////////////////////////////
package com.gitee.dbswitch.admin.model.request;

import com.gitee.dbswitch.admin.common.excption.DbswitchException;
import com.gitee.dbswitch.admin.common.response.ResultCode;
import com.gitee.dbswitch.admin.entity.AssignmentConfigEntity;
import com.gitee.dbswitch.admin.entity.AssignmentTaskEntity;
import com.gitee.dbswitch.admin.service.ScheduleService;
import com.gitee.dbswitch.admin.type.IncludeExcludeEnum;
import com.gitee.dbswitch.admin.type.ScheduleModeEnum;
import java.util.List;
import java.util.Objects;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class AssigmentUpdateRequest {

  private Long id;
  private String name;
  private String description;
  private ScheduleModeEnum scheduleMode;
  private String cronExpression;
  private AssigmentCreateConfig config;

  @NoArgsConstructor
  @Data
  public static class AssigmentCreateConfig {

    private Long sourceConnectionId;
    private String sourceSchema;
    private IncludeExcludeEnum includeOrExclude;
    private List<String> sourceTables;
    private Long targetConnectionId;
    private String targetSchema;
    private String tablePrefix;
    private Boolean targetDropTable;
    private Integer batchSize;
  }


  public AssignmentTaskEntity toAssignmentTask() {
    AssignmentTaskEntity assignment = new AssignmentTaskEntity();
    assignment.setId(id);
    assignment.setName(name);
    assignment.setDescription(description);
    assignment.setScheduleMode(scheduleMode);
    if (ScheduleModeEnum.SYSTEM_SCHEDULED == this.getScheduleMode()) {
      if (!ScheduleService.checkCronExpressionValid(this.getCronExpression())) {
        throw new DbswitchException(ResultCode.ERROR_INVALID_ARGUMENT, this.getCronExpression());
      }

      assignment.setCronExpression(this.getCronExpression());
    }

    return assignment;
  }

  public AssignmentConfigEntity toAssignmentConfig(Long assignmentId) {
    if (config.getSourceConnectionId() == config.getTargetConnectionId()) {
      throw new DbswitchException(ResultCode.ERROR_INVALID_ASSIGNMENT_CONFIG, "源端与目标端不能相同");
    }

    AssignmentConfigEntity assignmentConfigEntity = new AssignmentConfigEntity();
    assignmentConfigEntity.setAssignmentId(assignmentId);
    assignmentConfigEntity.setSourceConnectionId(this.getConfig().getSourceConnectionId());
    assignmentConfigEntity.setSourceSchema(this.getConfig().getSourceSchema());
    assignmentConfigEntity.setSourceTables(this.getConfig().getSourceTables());
    assignmentConfigEntity.setExcluded(
        this.getConfig().getIncludeOrExclude() == IncludeExcludeEnum.EXCLUDE
    );
    assignmentConfigEntity.setTargetConnectionId(this.getConfig().getTargetConnectionId());
    assignmentConfigEntity.setTargetSchema(this.getConfig().getTargetSchema());
    assignmentConfigEntity.setTablePrefix(this.getConfig().getTablePrefix());
    assignmentConfigEntity.setTargetDropTable(this.getConfig().getTargetDropTable());
    assignmentConfigEntity.setBatchSize(
        Objects.isNull(this.config.getBatchSize())
            ? 10000
            : this.config.getBatchSize()
    );
    assignmentConfigEntity.setFirstFlag(Boolean.TRUE);

    return assignmentConfigEntity;
  }
}
